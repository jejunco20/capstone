import os
import glob
import re
import heapq
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import torch
from collections import defaultdict



path = os.path.expanduser('~/Capstone/Data')
# Load GPT-2 model and tokenizer
model = GPT2LMHeadModel.from_pretrained("gpt2")
tokenizer = GPT2Tokenizer.from_pretrained("gpt2")

def remove_html_tags(text):
    clean_text = re.sub('<.*?>', '', text)
    return clean_text

def split_into_sentences(text):
    return re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', text)

def get_sentence_score(sentence, model, tokenizer):
    inputs = tokenizer(sentence, return_tensors="pt")
    with torch.no_grad():
        outputs = model(**inputs)
        logits = outputs.logits
    return logits[0, -1, :].max().item()

def extractive_summary(text, model, tokenizer, num_sentences=3):
    sentences = split_into_sentences(text)
    sentence_scores = [(get_sentence_score(sentence, model, tokenizer), sentence) for sentence in sentences]
    top_sentences = heapq.nlargest(num_sentences, sentence_scores)
    summary = " ".join([sentence for _, sentence in top_sentences])
    return summary

def extract_info_from_file(filepath):
    # Initialize dictionary to hold results for each document
    results = defaultdict(list)
    # Initialize variables to hold document ID and text
    doc_id = ''
    text = ''
    # Open file for reading
    with open(filepath, 'r') as f:
        # Loop through each line in the file
        for line in f:
            # Check for start of new document
            if '<DOCID>' in line:
                # If starting new document, extract document ID
                doc_id = line.strip().replace('<DOCID>', '').replace('</DOCID>', '')
                # Reset text variable
                text = ''
            # Check for end of document
            elif '</DOC>' in line:
                # If end of document, extract named entities and sentences with named entities
                text = remove_html_tags(text)
                summary = extractive_summary(text, model, tokenizer, num_sentences=3)
                # Add results to dictionary
                results[doc_id] = summary
            # Otherwise, add line to text variable
            else:
                text += line.strip()
    return results

def extract_info_from_directory(directory_path):
    results = []
    count = 0
    for filepath in glob.glob(os.path.join(directory_path, 'LA*')):
        count+=1
        file_results = extract_info_from_file(filepath)
        for doc_sentences in file_results.values():
            results.extend(doc_sentences)
        if count ==1:
            break
    return results


result = extract_info_from_directory(path)
# text = "Your input text here."
# summary = extractive_summary(text, model, tokenizer, num_sentences=3)
print(result)
