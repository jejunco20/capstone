import glob
import os
import re
import spacy
import numpy as np
from collections import defaultdict
from gensim.parsing.preprocessing import preprocess_string
from gensim.corpora import Dictionary

nlp = spacy.load('en_core_web_sm')

# Define function to remove HTML tags from text
def remove_html_tags(text):
    clean_text = re.sub('<.*?>', '', text)
    return clean_text

# Define function to preprocess text using gensim
def preprocess(text):
    return preprocess_string(text)

# Define function to extract named entities and sentences with named entities from each document in a file
def extract_info_from_file(filepath):
    # Initialize dictionary to hold results for each document
    results = defaultdict(list)
    # Initialize variables to hold document ID and text
    doc_id = ''
    text = ''
    # Open file for reading
    with open(filepath, 'r') as f:
        # Loop through each line in the file
        for line in f:
            # Check for start of new document
            if '<DOCID>' in line:
                # If starting new document, extract document ID
                doc_id = line.strip().replace('<DOCID>', '').replace('</DOCID>', '')
                # Reset text variable
                text = ''
            # Check for end of document
            elif '</DOC>' in line:
                # If end of document, extract named entities and sentences with named entities
                text = remove_html_tags(text)
                doc = nlp(text)
                named_entities = [ent.text for ent in doc.ents if ent.label_ != 'CARDINAL']
                sentences_with_named_entity = [sent.text for sent in doc.sents if any(ent.text in sent.text for ent in doc.ents if ent.label_ != 'CARDINAL')]
                preprocessed_sentences = [preprocess(sent) for sent in sentences_with_named_entity]
                dictionary = Dictionary(preprocessed_sentences)
                corpus = [dictionary.doc2bow(sentence) for sentence in preprocessed_sentences]
                vectorized_sentences = [[count for _, count in sentence] for sentence in corpus]
                # Add results to dictionary
                results[doc_id] = vectorized_sentences
            # Otherwise, add line to text variable
            else:
                text += line.strip()
    return results

# # Define function to extract named entities and sentences with named entities from all files in a directory
# def extract_info_from_directory(directory_path):
#     results = {}
#     # Find all files in directory that match pattern 'LA*'
#     for filepath in glob.glob(os.path.join(directory_path, 'LA*')):
#         # Extract information from each file and add to results dictionary
#         file_results = extract_info_from_file(filepath)
#         results.update(file_results)
#     return results

# # Example usage
# directory_path = os.path.expanduser('~/Capstone/Data')
# results = extract_info_from_directory(directory_path)
# print(results)
def extract_info_from_directory(directory_path):
    results = []
    for filepath in glob.glob(os.path.join(directory_path, 'LA*')):
        file_results = extract_info_from_file(filepath)
        for doc_sentences in file_results.values():
            results.extend(doc_sentences)
    return results

# Example usage
directory_path = os.path.expanduser('~/Capstone/Data')
named_entity_features = extract_info_from_directory(directory_path)

# Convert the list of lists into a numpy array
named_entity_features = np.array(named_entity_features, dtype=object)
