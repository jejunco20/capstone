from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lex_rank import LexRankSummarizer
from gensim.utils import simple_preprocess
import os
import glob
import re

path = os.path.expanduser('~/Capstone/Data')

def get_topic_sentences(file_path):
    topic_sentences = {}

    for file in glob.glob(os.path.join(file_path, 'LA*')):
        with open(file, 'r') as f:
            content = f.read()
            articles = re.split(r'<DOC>', content)

            for article in articles[1:]:
                article_num = int(re.search(r'<DOCID>\s+(\d+)\s+</DOCID>', article).group(1))
                article_content = re.sub(r'<[^>]*>', '', article)

                # Parse the article_content using sumy's PlaintextParser
                parser = PlaintextParser.from_string(article_content, Tokenizer('english'))

                # Initialize the LexRank summarizer and set the number of sentences to extract
                summarizer = LexRankSummarizer()
                summarizer.stop_words = [' ']  # set stop words to an empty list
                num_sentences = 1  # number of sentences to extract for each paragraph

                # Extract the topic sentence for each paragraph using LexRank
                article_sentences = []
                for paragraph in parser.document.paragraphs:
                    if len(paragraph.sentences) > 0:
                        summary = summarizer(paragraph, num_sentences)
                        topic_sentence = str(summary[0])
                        article_sentences.append(topic_sentence)

                topic_sentences[article_num] = article_sentences

    return topic_sentences

def tokenize_topic_sentences(topic_sentences):
    tokenized_sentences = {}

    for article_num, sentences in topic_sentences.items():
        tokenized_sentences[article_num] = [simple_preprocess(sentence) for sentence in sentences]

    return tokenized_sentences

topic_sentences = get_topic_sentences(path)

# Tokenize the topic sentences
tokenized_topic_sentences = tokenize_topic_sentences(topic_sentences)

# Convert the tokenized topic sentences to a list of lists
topic_sentence_list = []
for sentences in tokenized_topic_sentences.values():
    topic_sentence_list.extend(sentences)
