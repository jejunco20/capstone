import os
import glob
import gensim as gm
import re
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

cue_words = ["in addition", "moreover", "besides", "furthermore", "however", "nevertheless", 
             "on the other hand", "nonetheless", "whereas", "although", "even though", "for example", "for instance", 
             "namely", "specifically", "to illustrate", "as a result", "consequently", "therefore", "thus", "hence", 
             "because", "since", "conclusion", "in summary", "overall", "to sum up", "to summarize", "meanwhile", 
             "during", "after", "before", "then, next", "eventually", "likewise, similarly", "in the same way", "just as", 
             "on the contrary", "on the other hand", "yet", "while", "although", "even though"]

path = os.path.expanduser('~/Capstone/Data')

tokenizer = Tokenizer()

def preprocess_file_content(file_content):
    processed_content = []
    article_pattern = re.compile(r"Article\d+")
    
    lines = file_content.split("\n")
    line = 0
    index = 0
    while line < len(lines):
        l = lines[line]
        if "<HEADLINE>" in l:
            t = []
            while "</HEADLINE>" not in l:
                if lines[line][0] == '<':
                    line += 1
                    l = lines[line]
                    continue
                else:
                    t.append(lines[line].lower().strip())
                    line += 1
            processed_content.append(f"Article{index}\n")
            processed_content.append('\n'.join(t) + '\n')
            index += 1
        else:
            processed_content.append(lines[line])
            line += 1
    
    return "\n".join(processed_content)

def match_cue_words(path, cue_words):
    file_tensor = {}
    article_pattern = re.compile(r"Article\d+")

    for file in glob.glob(os.path.join(path, 'LA*')):
        i = 0
        with open(file, 'r') as f:
            file_content = f.read()
        
        preprocessed_content = preprocess_file_content(file_content)
        
        for line in preprocessed_content.split("\n"):
            if not line or line[0] == '<':
                continue
            elif article_pattern.search(line):
                file_tensor[f"title{i}"] = {"tokenized_sentences": [], "cue_word_features": []}
                i += 1
            else:
                x = " ".join(gm.utils.simple_preprocess(line))
                tokenizer.fit_on_texts([x])
                tokenized_sentence = tokenizer.texts_to_sequences([x])[0]
                cue_word_feature = int(any(word.lower() in x.lower() for word in cue_words))
                file_tensor[f"title{i-1}"]["tokenized_sentences"].append(tokenized_sentence)
                file_tensor[f"title{i-1}"]["cue_word_features"].append(cue_word_feature)
                
    max_sentence_length = max([len(seq) for article in file_tensor.values() for seq in article["tokenized_sentences"]])
    
    for article in file_tensor.values():
        article["tokenized_sentences"] = pad_sequences(article["tokenized_sentences"], maxlen=max_sentence_length)
    
    return file_tensor, max_sentence_length
