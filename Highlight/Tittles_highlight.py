from colorama import init, Fore, Back, Style
import os
import glob
import gensim as gm
from keras.preprocessing.text import Tokenizer
import numpy as np

def get_titles(path):
    titles = []
    for file in glob.glob(os.path.join(path, 'LA*')):
        with open(file, 'r') as f:
            lines = f.readlines()

        line = 0
        while line < len(lines):
            l = lines[line]
            if "<HEADLINE>" in l:
                t = []
                while "</HEADLINE>" not in l:
                    if lines[line][0] == '<':
                        line += 1
                        l = lines[line]
                        continue
                    else:
                        t.append(lines[line].lower().strip())
                        line += 1
                titles.append(t)
            else:
                line += 1

    pruned_titles = []
    if titles:
        titles = [' '.join(title) for title in titles]
        pruned_titles = [" ".join(gm.utils.simple_preprocess(title)) for title in titles]

    return pruned_titles

# The rest of the code remains the same

from keras.preprocessing.text import Tokenizer

def preprocess_file_content(file_content, titles):
    processed_content = []

    lines = file_content.split("\n")
    line = 0
    index = 0
    while line < len(lines):
        l = lines[line]
        if "<HEADLINE>" in l:
            t = []
            while "</HEADLINE>" not in l:
                if lines[line][0] == '<':
                    line += 1
                    l = lines[line]
                    continue
                else:
                    t.append(lines[line].lower().strip())
                    line += 1
            processed_content.append(f"Article{index}\n")
            index += 1
        else:
            processed_content.append(lines[line])
            line += 1

    return "\n".join(processed_content)

def match_titles(path, titles):
    file_tensor = {}
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(titles)

    for i in range(len(titles)):
        file_tensor[f"title{i}"] = []

    for file in glob.glob(os.path.join(path, 'LA*')):
        i = 0
        with open(file, 'r') as f:
            file_content = f.read()

        preprocessed_content = preprocess_file_content(file_content, titles)

        highlighted_content = ['<!DOCTYPE html>', '<html>', '<head>', '<style>',
                               'mark {background-color: yellow;}', '</style>', '</head>', '<body>']
        for line in preprocessed_content.split("\n"):
            if not line or line[0] == '<':
                highlighted_content.append(line)
                continue
            elif i < len(titles) and any(word.lower() in line.lower() for word in titles[i].split()):
                title_features = [int(word.lower() in line.lower()) for word in tokenizer.word_index.keys()]
                file_tensor[f"title{i}"].append(title_features)
                # Highlight the line
                line = f'<mark>{line}</mark>'
            highlighted_content.append(line)
            if i < len(titles) - 1 and any(word.lower() in line.lower() for word in titles[i + 1].split()):
                i += 1
        highlighted_content.extend(['</body>', '</html>'])

        # Save the highlighted content to a new HTML file
        with open(file + '.highlighted.html', 'w') as f:
            f.write('\n'.join(highlighted_content))

    return file_tensor

titles = get_titles(path)
file_tensor = match_titles(path, titles)
