import glob
import os
import re
import spacy
import numpy as np
from collections import defaultdict
from gensim.parsing.preprocessing import preprocess_string
from gensim.corpora import Dictionary

nlp = spacy.load('en_core_web_sm')

def remove_html_tags(text):
    clean_text = re.sub('<.*?>', '', text)
    return clean_text

def preprocess(text):
    return preprocess_string(text)

def highlight_named_entities(text):
    doc = nlp(text)
    for ent in doc.ents:
        if ent.label_ != 'CARDINAL':
            text = text.replace(ent.text, f'<mark>{ent.text}</mark>')
    return text

def extract_info_from_file(filepath):
    results = defaultdict(list)
    doc_id = ''
    text = ''

    with open(filepath, 'r') as f:
        for line in f:
            if '<DOCID>' in line:
                doc_id = line.strip().replace('<DOCID>', '').replace('</DOCID>', '')
                text = ''
            elif '</DOC>' in line:
                text = remove_html_tags(text)
                highlighted_text = highlight_named_entities(text)
                with open(filepath + f'.{doc_id}.highlighted.html', 'w') as hf:
                    hf.write(highlighted_text)
                text = ''
            else:
                text += line.strip()
    return results

def extract_info_from_directory(directory_path):
    results = []
    for filepath in glob.glob(os.path.join(directory_path, 'LA*')):
        file_results = extract_info_from_file(filepath)
        for doc_sentences in file_results.values():
            results.extend(doc_sentences)
    return results

directory_path = os.path.expanduser('~/Capstone/Data')
named_entity_features = extract_info_from_directory(directory_path)
named_entity_features = np.array(named_entity_features, dtype=object)
