# Capstone

Identifying the most relevant text information from online texts has become a standard task now that data is easily accessible. Conse- quently, summary generation has gained significant importance by reducing the amount of text a person has to read t

# Extractive Summarization with a Neural Network

This code implements an extractive summarization system using a neural network. The system takes an input text and generates a summary of the text. The summary consists of the sentences with the highest probability scores. The threshold value can be adjusted to control the length of the summary.

The code is divided into the following files:

- preprocess.py: This file contains the functions for preprocessing the input text.
- model.py: This file contains the code for the neural network.
- generate_summary.py: This file contains the code for generating the summary.

To use the code, you will need to install the following packages:

- numpy
- keras

Once you have installed the packages, you can run the code as follows:

```
python generate_summary.py input.txt

The output of the code will be a list of sentences that form the summary of the input text.
```
# Parameters

The following parameters can be adjusted to control the behavior of the system:

- vocab_size: The size of the vocabulary.
- embedding_dim: The dimension of the word embeddings.
- max_sentence_length: The maximum length of a sentence.
- threshold: The threshold value for selecting sentences for the summary.

# Example

The following is an example of how to use the code:
```
python generate_summary.py input.txt
```
The output of the code will be a list of sentences that form the summary of the input text. For example, if the input text is the following:
```
This is a sample text. It is used to demonstrate the extractive summarization system.
```
The output of the code will be the following:
```
This is a sample text.
It is used to demonstrate the extractive summarization system.
```
